FROM confluentinc/cp-kafka:6.2.0

# Copy the configuration file
COPY ./server.properties /etc/kafka/server.properties

# Expose Kafka port
EXPOSE 9092
